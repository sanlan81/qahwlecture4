package assignment4.tests;


import assignment4.BaseTest;
import assignment4.GeneralActions;
import assignment4.model.ProductData;
import assignment4.utils.logging.WebDriverLogger;
import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static assignment4.model.ProductData.generate;
import static assignment4.utils.Properties.getBaseAdminUrl;

public class CreateProductTest extends BaseTest {

    EventFiringWebDriver driver;
    GeneralActions generalActions;

    @Test(dataProvider = "SearchProvider")
    public void createNewProduct(String login, String password) throws InterruptedException {

        driver = new EventFiringWebDriver(getDriver("chrome"));

        driver.register(new WebDriverLogger());
        BasicConfigurator.configure();

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(getBaseAdminUrl());

        driver.manage().window().maximize();

        // login
        generalActions = new GeneralActions(driver);
        generalActions.login(login, password);

        // main and sub menu
        generalActions.productsItem("subtab-AdminCatalog");

        generalActions.clickButton("page-header-desc-configuration-add");

        // product creation
        ProductData productData = generate();
        generalActions.createProduct(productData);

        //active product
        generalActions.activeProduct("//input[@id='form_step1_active']/..");
        generalActions.confirmationModal("growl-close");

        // save product
        generalActions.saveProduct("//span[text()='Сохранить']/..");
        generalActions.confirmationModal("growl-close");

        // visibility product
        generalActions.mainMenu("logo");

        // main and sub menu
        generalActions.productsItem("subtab-AdminCatalog");

        //check that product is on page
        Assert.assertTrue(generalActions.getProductByName("//a[contains(text(), '" + productData.getName() + "')]/..").size() > 0, "nonconformity of products");

        //Open product
        generalActions.toSeeProduct("//a[contains(text(), '" + productData.getName() + "')]");

        // Expected equals names
        Assert.assertEquals(productData.getName(), generalActions.nameOfProduct(), "different names  of products");

        // Expected equals quantity
        Assert.assertEquals(productData.getQty(), generalActions.quantityOfProduct(), "different quantity  of products");

        // Expected equals prices
        Assert.assertEquals(productData.getPrice(), generalActions.priceOfProduct(), "different prices  of products " + generalActions.priceOfProduct());

    }


    @DataProvider(name = "SearchProvider")
    public Object[][] getDataFromDataprovider() {
        return new Object[][]
                {{"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}};
    }

    // finish script
    @AfterClass
    public void afterTest() {
        driver.quit();
    }
}
