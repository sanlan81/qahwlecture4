package assignment4;

import assignment4.model.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Logs in to Admin Panel.
     *
     * @param login
     * @param password
     */
    public void login(String login, String password) {

        WebElement emailField = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("email"))));
        emailField.sendKeys(login);

        WebElement passwordField = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("passwd"))));
        passwordField.sendKeys(password);

        WebElement submitLogin = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.name("submitLogin"))));
        submitLogin.click();
    }

    public void productsItem(String click) throws InterruptedException {
        Actions builder = new Actions(driver);
        WebElement mainMenu = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(click))));
        builder.moveToElement(mainMenu).build().perform();
        WebElement subMenu = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[contains(text(),'товары')]/..")))); //Find the submenu
        builder.moveToElement(subMenu).click().build().perform();
    }

    // TODO manager for clicking
    public void clickButton(String click) {
        WebElement clickButton = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(click))));
        clickButton.click();
    }

    public void createProduct(ProductData newProduct) {
        WebElement productName = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form_step1_name_1"))));
        productName.sendKeys(newProduct.getName());
        WebElement productQty = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form_step1_qty_0_shortcut"))));
        productQty.sendKeys(newProduct.getQty() + "");
        WebElement productPrice = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form_step1_price_ttc_shortcut"))));
        productPrice.sendKeys(newProduct.getPrice() + "");
    }

    public void activeProduct(String activeProduct) {
        WebElement activationOfProduct = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(activeProduct))));
        activationOfProduct.click();
    }

    public void confirmationModal(String confirmation_modal) {
        WebElement closeModalWindow = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className(confirmation_modal))));
        closeModalWindow.click();
    }

    public void saveProduct(String newProduct) {
        WebElement addingNewProduct = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(newProduct))));
        addingNewProduct.click();
    }

    public void mainMenu(String logo) {
        WebElement mainPage = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className(logo))));
        mainPage.click();
    }

    public List<WebElement> getProductByName(String productName) {

        return driver.findElements(By.xpath(productName));
    }

    public void toSeeProduct(String productName) {
        WebElement product = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(productName))));
        product.click();
    }

    public String nameOfProduct() {
        return wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//input[@id='form_step1_name_1']")))).getAttribute("value");
    }

    public String priceOfProduct() {
        return wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form_step1_price_ttc_shortcut")))).getAttribute("value");
    }

    public Integer quantityOfProduct() {
        return Integer.valueOf(wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("form_step1_qty_0_shortcut")))).getAttribute("value"));
    }
}



